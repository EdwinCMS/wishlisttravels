import { DestinoViajes} from './destino-viajes';
import { ThrowStmt } from '@angular/compiler';
import { Subject, BehaviorSubject } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { NuevoDestinoAction, ElegidoFavoritoAction } from './destino-viajes-state';
import { Injectable } from '@angular/core';


@Injectable()
export class DestinosApiClient {
    
    constructor(private store: Store<AppState>){
    }

    add(d: DestinoViajes){
        this.store.dispatch(new NuevoDestinoAction(d));
    }  

    elegir(d: DestinoViajes){
        this.store.dispatch(new ElegidoFavoritoAction(d));
    }

}
