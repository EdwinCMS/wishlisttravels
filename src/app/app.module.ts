import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule  }from '@angular/forms';
import { StoreModule as NgRxStoreModule, ActionReducerMap, StoreModule }from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DestinoViajesComponent } from './components/destino-viajes/destino-viajes.component';
import { ListaDestinosComponent } from './components/lista-destinos/lista-destinos.component';
import { DestinoDetallesComponent } from './components/destino-detalles/destino-detalles.component';
import { FormDestinoViajesComponent } from './components/form-destino-viajes/form-destino-viajes.component';
import { DestinosApiClient } from './models/destinos-api-client';
import { DestinosViajesState, reducerDestinosViajes, intializeDestinosViajesState,DestinosViajesEffects} from './models/destino-viajes-state';
import { LoginComponent } from './components/login/login.component';
import { ProtectedComponent } from './components/protected/protected.component';

import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { AuthService } from './services/auth.service';
import { VuelosComponentsComponent } from './components/vuelos/vuelos-components/vuelos-components.component';
import { VuelosMainComponentsComponent } from './components/vuelos/vuelos-main-components/vuelos-main-components.component';
import { VuelosMasInfoComponentsComponent } from './components/vuelos/vuelos-mas-info-components/vuelos-mas-info-components.component';
import { VuelosDetalleComponentsComponent } from './components/vuelos/vuelos-detalle-components/vuelos-detalle-components.component';
import { ReservasModule } from './reservas/reservas.module';



//Rutas

// init routing
export const childrenRoutesVuelos: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'main', component: VuelosMainComponentsComponent },
  { path: 'mas-info', component: VuelosMasInfoComponentsComponent },
  { path: ':id', component: VuelosDetalleComponentsComponent },
];
const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: ListaDestinosComponent },
  { path: 'destino/:id', component: DestinoDetallesComponent },
  { path: 'login', component: LoginComponent },
  {
    path: 'protected',
    component: ProtectedComponent,
    canActivate: [ UsuarioLogueadoGuard ]
  },
  {
    path: 'vuelos',
    component: VuelosComponentsComponent,
    canActivate: [ UsuarioLogueadoGuard ],
    children: childrenRoutesVuelos
  }
];

// redux init
  export interface AppState {
    destinos: DestinosViajesState;
  }
  const reducers: ActionReducerMap<AppState> = {
    destinos: reducerDestinosViajes
  };

  const reducersInitialState = {
    destinos: intializeDestinosViajesState()
  };
// fin redux init


@NgModule({
  declarations: [
    AppComponent,
    DestinoViajesComponent,
    ListaDestinosComponent,
    DestinoDetallesComponent,
    FormDestinoViajesComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosComponentsComponent,
    VuelosMainComponentsComponent,
    VuelosMasInfoComponentsComponent,
    VuelosDetalleComponentsComponent,

    
  ],
  imports: [
    BrowserModule,
    //Formularios
    FormsModule,
    ReactiveFormsModule,
    //Rutas
    //registro de las rutas, vinculacion al decorador @NgModule
    AppRoutingModule,
    RouterModule.forRoot(routes),    
    NgRxStoreModule.forRoot(reducers, { 
      initialState: reducersInitialState,
      runtimeChecks: {
        strictStateImmutability: false,
        strictActionImmutability: false,
      } }),
    EffectsModule.forRoot([DestinosViajesEffects]),
    StoreDevtoolsModule.instrument(),
    ReservasModule
  ],
  providers: [
    DestinosApiClient,
    AuthService,
    UsuarioLogueadoGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
