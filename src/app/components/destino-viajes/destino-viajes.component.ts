import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core'; //new: Input: permite obtener el valor de una variabe de afuera
import { DestinoViajes } from '../../models/destino-viajes';
import { DestinosApiClient } from '../../models/destinos-api-client';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { VoteUpAction, VoteDownAction } from '../../models/destino-viajes-state';

@Component({
  selector: 'app-destino-viajes',
  templateUrl: './destino-viajes.component.html',
  styleUrls: ['./destino-viajes.component.css']
})
export class DestinoViajesComponent implements OnInit {
  @Input() destino: DestinoViajes;
  @Input("idx") position: number;
  @HostBinding('attr.class') cssClass = 'col-md-4';//vinculacion directa con el tag
  @Output() clicked: EventEmitter<DestinoViajes>; //creacion nuevo disparador de evento
  constructor(private store: Store<AppState>) {
    this.clicked = new EventEmitter();//instancia de nuevo evento
  }

  ngOnInit(): void {
  }
  ir(){
    this.clicked.emit(this.destino);
    return false; //para que no recargue la pagina
  }
  voteUp(){
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }

  voteDown(){
    this.store.dispatch(new VoteDownAction(this.destino));
    return false;
  }
}
