import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VuelosDetalleComponentsComponent } from './vuelos-detalle-components.component';

describe('VuelosDetalleComponentsComponent', () => {
  let component: VuelosDetalleComponentsComponent;
  let fixture: ComponentFixture<VuelosDetalleComponentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VuelosDetalleComponentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VuelosDetalleComponentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
