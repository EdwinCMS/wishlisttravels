import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-vuelos-detalle-components',
  templateUrl: './vuelos-detalle-components.component.html',
  styleUrls: ['./vuelos-detalle-components.component.css']
})
export class VuelosDetalleComponentsComponent implements OnInit {
  
    id: any;
  
    constructor(private route: ActivatedRoute) {
      route.params.subscribe(params => { this.id = params['id']; });
    }
    ngOnInit() {
    }
    
}
