import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { DestinoViajes } from '../../models/destino-viajes';
import { FormGroup, FormBuilder, Validators, FormControl, ControlContainer, ValidatorFn } from '@angular/forms'
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax, AjaxResponse } from 'rxjs/ajax';

@Component({
  selector: 'app-form-destino-viajes',
  templateUrl: './form-destino-viajes.component.html',
  styleUrls: ['./form-destino-viajes.component.css']
})
export class FormDestinoViajesComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViajes>;
  fg: FormGroup;
  minLongitud = 3;
  searchResults: string[];

  constructor(fb: FormBuilder) {
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      //Aqui se llaman los validadores
      //'compose': coleccion de validaciones
      nombre: ['', Validators.compose([
        Validators.required,
        this.nombreValidator,
        this.nombreValidatorParametizable(this.minLongitud),
      ])],
      url: ['']
    })
   }

  ngOnInit(): void {
    let elemNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemNombre, 'input')
    //eventos de tecleado
      .pipe(
        map((e: KeyboardEvent)=>(e.target as HTMLInputElement).value),
        //captura de 2 caracteres o mas
        filter(text => text.length > 2),
        //stop
        debounceTime(200),
        distinctUntilChanged(),
        switchMap(()=> ajax('/assets/datos.json'))
      ).subscribe(AjaxResponse => {
        this.searchResults = AjaxResponse.response;
      });
  }
  guardar(nombre: string, url: string): boolean{
    const d = new DestinoViajes(nombre, url);
    this.onItemAdded.emit(d);
    return false;
  }
  
  nombreValidator(control: FormControl):{ [s: string]:boolean }{
    const l = control.value.toString().trim().length;
    if(l>0 && l<5){
      return { invalidNombre: true };
    }
    return null;
  }
 nombreValidatorParametizable(minLong: number): ValidatorFn{
  return(control: FormControl): {[s:string]:boolean}| null => {const l = control.value.toString().trim().length;
    if(l>0 && l<minLong){
      return { minLongNombre: true };
    }
  return null;
  }
 } 
}
